/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05_ex02;
import java.util.Scanner;
        
public class Ficha05_Ex02 {
    
    static Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       double cateto1 = 0, cateto2 = 0, hipotenusa = 0;
        
       System.out.println("Introduza o valor do primeiro cateto: ");
       cateto1 = scanner.nextDouble();
       
       System.out.println("Introduza o valor do segundo cateto: ");
       cateto2 = scanner.nextDouble();
       
       hipotenusa =  Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2) );
       
       System.out.println("Valor da hipotenusa é: " + hipotenusa); 
       
    }
    
}
