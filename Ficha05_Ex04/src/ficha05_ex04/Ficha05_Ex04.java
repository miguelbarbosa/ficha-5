/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05_ex04;
import java.util.Scanner;
/**
 *
 * @author 1181339
 */
public class Ficha05_Ex04 {
    static Scanner scanner = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        double total;
        
        System.out.println("Quantos hamburguers comeu ?: ");
        total = scanner.nextDouble() * 3; 
        
        System.out.println("Quantos cheesburguers comeu ?: ");
        total = total + scanner.nextDouble() * 2.50;
        
        System.out.println("Quantos batatas fritas comeu ?: ");
        total = total + scanner.nextDouble() * 1.50;
        
        System.out.println("Quantos refrigerantes comeu ?: ");
        total = total + scanner.nextDouble() * 1.10;
        
        System.out.println("Quantos milkshakes comeu ?: ");
        total = total + scanner.nextDouble() * 2.30;
        
        System.out.println("Quantos gelados comeu ?: ");
        total = total + scanner.nextDouble() * 2.50;
        
        System.out.println("O total é de : " + total);   
        
    }
   
    
}
