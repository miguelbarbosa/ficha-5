/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05_ex07;
    import java.util.Scanner;
/**
 *
 * @author User
 */
public class Ficha05_Ex07 {
    static Scanner scanner = new Scanner (System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        double MiniTeste, Trabalhos, Nota_Aulas, CAD ;
        int N_BI, N_Aluno, Aulas_Ass, Aulas_Dadas;
        
        
        System.out.println("Introduza o nº do bilhete de identidade do aluno: ");
        N_BI = scanner.nextInt();
        
        System.out.println("Qual o número do aluno");
        N_Aluno = scanner.nextInt();
        
        System.out.println ("Nota do primeiro mini-teste: ");
        MiniTeste = scanner.nextDouble();
        
        System.out.println ("Nota do segundo mini-teste: ");
        MiniTeste = scanner.nextDouble() + MiniTeste;
        
        System.out.println ("Nota do terceiro mini-teste: ");
        MiniTeste = scanner.nextDouble() + MiniTeste;
        
        MiniTeste = (MiniTeste / 3) * 0.5; 
        
        System.out.println ("Nota do primeiro trabalho: ");
        Trabalhos = scanner.nextDouble() * 0.4;
        
        System.out.println ("Nota do segundo trabalho: ");
        Trabalhos = (scanner.nextDouble() * 0.6) + Trabalhos;
        
        Trabalhos = Trabalhos * 0.4; 
        
        System.out.println ("Quantas aulas o aluno assistiu ?: ");
        Aulas_Ass = scanner.nextInt();
        
        System.out.println ("Quantas aulas totais foram dadas ?: ");
        Aulas_Dadas = scanner.nextInt();
        
        Nota_Aulas = ((Aulas_Ass / Aulas_Dadas) * 20) * 0.1;
        
        CAD = MiniTeste + Trabalhos + Nota_Aulas;
        
        System.out.println("Classificação Avaliação Distribuida é de: " + CAD);
                
    }
    
}
